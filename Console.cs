﻿//using Google.Apis.Auth.OAuth2;
//using Google.Apis.Discovery;
//using Google.Apis.Admin.Directory.directory_v1;
//using Google.Apis.Admin.Directory.directory_v1.Data;
//using Google.Apis.Services;
//using Google.Apis.Util.Store;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Threading;
//using System.Net.Http;
//using Newtonsoft.Json;

//namespace AdminSDKDirectoryQuickstart
//{
//    class Program
//    {
//        // If modifying these scopes, delete your previously saved credentials
//        // at ~/.credentials/admin-directory_v1-dotnet-quickstart.json
//        static string[] Scopes = { DirectoryService.Scope.AdminDirectoryUserReadonly, DirectoryService.Scope.AdminDirectoryGroupMember };

//        static string ApplicationName = "Directory API .NET Quickstart";

//        static void Main(string[] args)
//        {
//            var scopes = new List<string>
//            {
//                DirectoryService.Scope.AdminDirectoryGroup,
//                DirectoryService.Scope.AdminDirectoryGroupMember,
//                "https://www.googleapis.com/auth/cloud-platform",
//                "https://www.googleapis.com/auth/admin.directory.group.member",
//                "https://www.googleapis.com/auth/admin.directory.group.readonly",
//                "https://www.googleapis.com/auth/admin.directory.group",
//                "https://www.googleapis.com/auth/admin.directory.rolemanagement"
//            };
//            UserCredential credential;


//            using (var stream =
//                new FileStream("creds.json", FileMode.Open, FileAccess.Read))
//            {
                
//                // The file token.json stores the user's access and refresh tokens, and is created
//                // automatically when the authorization flow completes for the first time.
//                string credPath = "token1.json";
//                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
//                    GoogleClientSecrets.Load(stream).Secrets,
//                    scopes,
//                    "user",
//                    CancellationToken.None,
//                    new FileDataStore(credPath, true)).Result;
//                Console.WriteLine("Credential file saved to: " + credPath);
//            }

//            // Create Directory API service.
//            var service = new DirectoryService(new BaseClientService.Initializer()
//            {
//                HttpClientInitializer = credential,
//                ApplicationName = ApplicationName,
//            });

//            var list = service.Groups.List();
//            list.MaxResults = 10;
//            list.Domain = "rezero.us";

//            Console.Read();
//        }
//    }
//}