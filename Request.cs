﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal_Backend
{
    public class Request
    {
        public String Name { get; set; }
        public Int32 Age { get; set; }
    }
}
