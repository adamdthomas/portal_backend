﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Portal_Backend.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UtilitiesController : ControllerBase
    {

        private readonly ILogger _logger;

        public UtilitiesController(ILogger<UtilitiesController> logger)
        {
            _logger = logger;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            // Sends a message to configured loggers, including the Stackdriver logger.
            // The Microsoft.AspNetCore.Mvc.Internal.ControllerActionInvoker logger will log all controller actions with
            // log level information. This log is for additional information.
            _logger.LogDebug("Values retrieved!");
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            return new string[] { "Mariano","Darrin", "Adam" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public string Post([FromBody] Request value)
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            //_logger.LogInformation("Value added");
            System.Threading.Thread.Sleep(2000);
            return "OK";
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        [HttpOptions]
        public void Options()
        {
            Response.Headers.Add("Access-Control-Allow-Origin", "*");
            Response.Headers.Add("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
            Response.Headers.Add("Access-Control-Max-Age", "86400");
            Response.Headers.Add("Access-Control-Allow-Headers", "content-type");
        }
    }
}
